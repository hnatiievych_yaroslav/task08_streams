package com.hnatiievych.controller;

import com.hnatiievych.model.task1.Task1;
import com.hnatiievych.model.task2.Command;
import com.hnatiievych.model.task2.CommandImp;
import com.hnatiievych.model.task3.Task3;
import com.hnatiievych.model.task4.Task4;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Controller {
    Logger logger = LogManager.getLogger(Controller.class);

    // Task1.1 getMaxValue
    public int getMaxvalue(int first, int second, int third) {
        Task1 task1 = (a, b, c) -> {
            if ((a > b) && (a > c)) {
                return a;
            } else if ((b > a) && (b > c)) {
                return b;
            } else {
                return c;
            }
        };
        return task1.accept(first, second, second);
    }

    //Task1.2 get averege value
    public int getAveregeValue(int firth, int second, int third) {
        Task1 task1 = (a, b, c) -> (a + b + c) / 3;
        return task1.accept(firth, second, third);
    }

    //Task2
    public String CommandInterface(String str) {
        Map<String, Command> commandMap = new HashMap<>();
        commandMap.put("lambda", s -> s + "first lambda command");
        commandMap.put("anonimus", new Command() {
            @Override
            public String execute(String str) {
                return "annonimus class";
            }
        });
        commandMap.put("reference", new CommandImp()::execute);
        commandMap.put("object", new CommandImp());
        return commandMap.get(str).execute(str);

    }

    //Task3
    public IntSummaryStatistics createListWithStatistic() {
        Task3 task3 = new Task3();
        return task3.getStatistaic(task3.createFirst());
    }

    //Task4
    public void getLinestatictic(String str) {
        logger.info("Count unique word:" + Task4.numberUniqueWord(str));
        logger.info("List unique word:");
        Task4.sortedListUniqueWord(str)
                .stream().forEach(System.out::println);

        logger.info("Count word:");
        Task4.getWordCount(str).entrySet().stream().forEach(System.out::println);

        logger.info("Count char:");
        Task4.getSymbolStatistic(str).entrySet().stream().forEach(System.out::println);
    }
}
