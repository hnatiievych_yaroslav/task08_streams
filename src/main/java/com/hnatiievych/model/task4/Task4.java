package com.hnatiievych.model.task4;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task4 {
    public static long numberUniqueWord(String str) {
        return Stream.of(str)
                .flatMap(e->Stream.of(e.split(" ")))
                .distinct()
                .count();
    }

    public static List<String> sortedListUniqueWord(String str) {
        return Stream.of(str)
                .flatMap(e->Stream.of(e.split(" ")))
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    public static Map<String, Long> getWordCount(String str) {
        return Stream.of(str).flatMap(e -> Stream.of(e.split(" ")))
                .collect(Collectors.groupingBy(e -> e, Collectors.counting()));
    }

    public static Map<String, Long> getSymbolStatistic(String str) {
        return Stream.of(str).flatMap(e -> Stream.of(e.split("")))
                .collect(Collectors.groupingBy(e -> e, Collectors.counting()));
    }
}
