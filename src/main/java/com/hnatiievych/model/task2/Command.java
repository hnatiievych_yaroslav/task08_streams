package com.hnatiievych.model.task2;

@FunctionalInterface
public interface Command {
    String execute(String str);
}
