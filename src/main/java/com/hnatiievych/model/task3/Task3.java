package com.hnatiievych.model.task3;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Task3 {
    List <Integer> list = new ArrayList<>();

    public List<Integer> createFirst (){
       return list = new Random()
               .ints()
               .limit(12)
               .boxed()
               .collect(Collectors.toList());
    }
    public IntSummaryStatistics getStatistaic (List<Integer> list) {
        return list.stream()
                .mapToInt(Integer::intValue)
                .summaryStatistics();
    }
}
