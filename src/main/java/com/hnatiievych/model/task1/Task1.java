package com.hnatiievych.model.task1;

@FunctionalInterface
public interface Task1 {
    public int accept(int a,int b,int c);
}
