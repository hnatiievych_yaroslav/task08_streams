package com.hnatiievych.view;

import com.hnatiievych.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class View {

    private Controller controller;
    private static Logger logger;

    public View() {
        controller = new Controller();
        logger = LogManager.getLogger(View.class);
    }

    public void show() {
        //task1
        logger.info("we have three number, like 4,6,1");
        logger.info("max value is: " + controller.getMaxvalue(4, 6, 1));
        logger.info("averege value is :" + controller.getAveregeValue(4, 6, 1));

        //task2

        logger.info("enter command name");
        Scanner scan = new Scanner(System.in);
        logger.info(controller.CommandInterface(scan.nextLine()));
        //task3
        logger.info("task3");

        logger.info("averege: "+controller.createListWithStatistic().getAverage());
        logger.info("max: " +controller.createListWithStatistic().getMax());
        logger.info("min: "+controller.createListWithStatistic().getMin());
        logger.info("sum: "+controller.createListWithStatistic().getSum());

        //task4
        logger.info("enter line and get statistic");
        Scanner scanner = new Scanner(System.in);
        StringBuilder lines = new StringBuilder();
        String tmp;

        do {
            lines.append(" ");
            tmp = scanner.nextLine();
            lines.append(tmp);
        } while (!tmp.equals(""));
        controller.getLinestatictic(lines.toString().trim());
    }
}
